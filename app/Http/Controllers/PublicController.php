<?php

namespace App\Http\Controllers;

use App\Models\article;
use Illuminate\Http\Request;
use App\Mail\CareerRequestMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class PublicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('homepage', 'wip', 'chiSiamo');
    }

    public function homepage()
    {
        $articles = Article::where('is_accepted', true)->orderBy('created_at', 'desc')->take(4)->get();
        return view('homepage', compact('articles'));
    }

    public function careers()
    {
        return view('careers');
    }

    public function careersSubmit(Request $request)
    {

        $request->validate([
            'role' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);
         

        $user = Auth::user();
        $role = $request->role;
        $email = $request->email;
        $message = $request->message;

        Mail::to('admin@theaulabpost.it')->send(new CareerRequestMail(compact('role', 'email', 'message')));

        switch ($role) {
            case 'admin':
                $user->is_admin = NULL;
                break;

            case 'revisor':
                $user->is_revisor = NULL;
                break;

            case 'writer':
                $user->is_writer = NULL;
                break;
        }

        $user->update();

        return redirect(route('homepage'))->with('message', 'Grazie per averci contattato!');
    }

    public function newsletter(){
        return view('components.footer');
    }
    
    public function newsletterSubmit(Request $request){
        $email = $request->input('email');

        $data = compact('email');

        Mail::to($email)->send(new NewsletterMail($data));

        return redirect(route('hompage'));
    }

    public function wip(){
        return view('wip');
    }

    public function chiSiamo(){
        return view('chiSiamo');
    }

}
