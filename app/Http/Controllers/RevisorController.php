<?php

namespace App\Http\Controllers;

use App\Models\article;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function dashboard(){
        $unrevisionedArticles = article::where('is_accepted', NULL)->get();
        $acceptedArticles = article::where('is_accepted', true)->get();
        $rejectedArticles = article::where('is_accepted', false)->get();

        return view('revisor.dashboard', compact('unrevisionedArticles', 'acceptedArticles', 'rejectedArticles'));
    }

    public function acceptArticle(article $article){
        $article->update([
            'is_accepted' => true,
        ]);

        return redirect(route('revisor.dashboard'))->with('message', 'Hai accettato l\'articolo scelto');
    }

    public function rejectArticle(article $article){
        $article->update([
            'is_accepted' => false,
        ]);

        return redirect(route('revisor.dashboard'))->with('message', 'Hai rifiutato l\'articolo scelto');
    }

    public function undoArticle(article $article){
        $article->update([
            'is_accepted' => NULL,
        ]);

        return redirect(route('revisor.dashboard'))->with('message', 'Hai riportato l\'articolo scelto di nuovo in revisione');
    }
}
