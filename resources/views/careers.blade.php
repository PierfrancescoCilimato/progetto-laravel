<x-form-layout>
    <div class="h-100 w-100 d-flex align-items-center justify-content-center">
            
                <form action="{{route('careers.submit')}}" method="POST" class="myRegForm w-75 fontLora p-4 mb-3 mx-5">
                    @csrf
                    <h3 class="text-center">Lavora con noi</h3>
                    <h5 class="text-center">Invia la tua richiesta</h5>
                    <hr>
                    <div class="mb-3">
                        <label for="role" class="form-label">Per quale ruolo ti stai candidando ?</label>
                        <select name="role" id="role" class="form-select">
                            <option value="">Scegli qui</option>
                            <option value="admin">Amministratore</option>
                            <option value="revisor">Revisore</option>
                            <option value="writer">Redattore</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input value="{{ old('email') ?? Auth::user()->email }}" name="email" type="email"
                            id="email" class="form-control">
                    </div>
                    <div class="mb-3 pb-5">
                        <label for="message" class="form-label">Parlaci di te</label>
                        <textarea name="message" id="message" cols="30" rows="7" class="form-control">{{ old('message') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-light formBtn px-5 fontLora">Invia candidatura</button>
                </form>
    </div>
</x-form-layout>
