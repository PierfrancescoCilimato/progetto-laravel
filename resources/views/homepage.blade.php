
<x-layout>

    <x-head>
        The Aulab Post
    </x-head>
    
    @if(session('message'))
    <div class="alert alert-success text-center">
        {{session('message')}}
    </div>
    @endif

    @if(session('emailSent'))
    <div class="alert alert-success text-center">
        {{session('emailSent')}}
    </div>
    @endif

    <div class="container my-5">
        <div class="row justify-content-around">
            @foreach ($articles as $article)
                <div class="col-12 col-lg-6 col-xl-3 mt-2">
                    
                    <x-card 
                        :article="$article"
                    />

                </div>
            @endforeach
        </div>
    </div>

</x-layout>