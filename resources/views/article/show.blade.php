<x-layout>
    <div class="container-fluid p-5 bgFooter text-center myColor1 navLog fontLora">
        <div class="container">

            <div class="row justify-content-center">
                <h1 class="display-3">{{ $article->title }}</h1>
            </div>
        </div>

    </div>



    <div class="container my-5">
        <div class="row justify-content-around fontLora">

            <div class="col-12 col-md-6">
                <img src="{{ Storage::url($article->image) }}" class="img-fluid rounded-4 showImg" />
            </div>
            <div class="col-12 col-md-6">
                <div class="text-center">
                    <h3>{{ $article->subtitle }}</h3>


                    <div class="text-muted my-3 fst-italic">
                        <p>
                            redatto il {{ $article->created_at->format('d/m/Y') }} da {{ $article->user->name }}
                        </p>
                    </div>
                </div>
                <p>{{$article->text}}</p>
                <a href="{{ route('article.index') }}" class="btn btn-dark text-white">Torna indietro</a>
                @if (Auth::user() && Auth::user()->is_revisor)
                    <a href="{{ route('revisor.acceptArticle', compact('article')) }}"
                        class="btn btn-outline-success fw-bold">Accetta articolo</a>
                    <a href="{{ route('revisor.rejectArticle', compact('article')) }}"
                        class="btn btn-outline-danger fw-bold">Rifiuta articolo</a>
                @endif
            </div>
        </div>

    </div>
</x-layout>

<?php

$showArticleDetails = true;

?>
