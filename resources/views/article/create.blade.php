<x-form-layout>

    <form action="{{route('article.store')}}" method="POST" enctype="multipart/form-data" class=" myForm m-auto fontLora p-4 mb-5">
        @csrf
        <!-- Name input -->
        <h3 class="text-center">Inserisci Articolo</h3>
        <div class="form-outline mb-4">
            <label class="form-label" for="title">Titolo</label>
            <input name="title" type="text" id="title" class="form-control" />
        </div>
        <!-- Email input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="subtitle">Sottotitolo</label>
            <input name="subtitle" type="text" id="subtitle" class="form-control" />
        </div>
        {{-- select category --}}
        <div class="mb-3">
            <label for="category" class="form-label">Categoria:</label> 
            <select name="category" id="category" class="form-control text-capitalize">
                @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="image" class="form-label">Immagine di copertina</label>
            <input name="image" class="form-control" type="file" id="image">
            </div>
        <!-- Message input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="text">Testo articolo</label>
            <textarea name="text" class="form-control" id="text" rows="4"></textarea>
        </div>
        
        <div class="mb-3 pb-2">
            <label for="tags" class="form-label">Tags</label>
            <input name="tags" id="tags" class="form-control" value="{{old('tags')}}">
            <span class="small fst-italic">Dividi ogni tag con una virgola</span>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-light fontLora formBtn px-5">Invia</button>
    </form>
</x-form-layout>