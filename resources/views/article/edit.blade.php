<x-form-layout>


    <form action="{{route('article.update', compact('article'))}}" method="POST" enctype="multipart/form-data" class=" myForm m-auto fontLora p-4 mb-3">
        @csrf
        @method('put')
        <!-- Name input -->
        <h3 class="text-center">Modifica Articolo</h3>

        <div class="form-outline mb-4">
            <label class="form-label" for="title">Titolo</label>
            <input name="title" type="text" id="title" class="form-control" value="{{$article->title}}"/>
        </div>
        <!-- Email input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="subtitle">Sottotitolo</label>
            <input name="subtitle" type="text" id="subtitle" class="form-control" value="{{$article->subtitle}}"/>
        </div>
        {{-- select category --}}
        <div class="mb-3">
            <label for="category" class="form-label">Categoria:</label> 
            <select name="category" id="category" class="form-control text-capitalize">
                @foreach ($categories as $category)
                    <option value="{{$category->id}}" @if ($article->category && $category->id == $article->category->id) selected @endif>{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="image" class="form-label" >Immagine di copertina</label>
            <input name="image" class="form-control" type="file" id="image">
            </div>
        <!-- Message input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="text">Testo articolo</label>
            <textarea name="text" class="form-control" id="text" rows="4" >{{$article->text}}</textarea>
        </div>
        <div class="mb-3">
            <input name="tags" id="tags" class="form-control" value="{{$article->tags->implode('name', ', ')}}">
            <label for="tags" class="form-label">Tags:</label>
            <span class="small fst-italic">Dividi ogni tag con una virgola</span>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-light fontLora formBtn px-5" >Invia modifiche</button>
    </form>
                    
</x-form-layout>