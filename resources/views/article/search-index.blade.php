<x-layout>
    <x-head>
        ricerca per: {{$query}}
    </x-head>
    <div class="container my-5">
        <div class="row justify-content-around">
            @foreach ($articles as $article)
                <div class="col-12 col-lg-6 col-xl-3 mt-2">
                    <x-card :article="$article" />
                </div>
            @endforeach
        </div>
    </div>

</x-layout>
