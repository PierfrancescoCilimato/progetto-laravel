<x-form-layout>
  
  <div class="h-100 w-100 d-flex align-items-center justify-content-center">
        <form method="POST" action="{{route('register')}}" class="myRegForm fontLora w-75 p-4 mb-3 mx-5 ">
            <!-- 2 column grid layout with text inputs for the first and last names -->
            @csrf

            <h3 class="text-center">Registrati</h3>

            <div class="form-outline mb-3">
              <label class="form-label" for="name">First name</label>
              <input name="name" type="text" id="name" class="form-control" />
            </div>

            <!-- Email input -->
            <div class="form-outline mb-3">
              <label class="form-label" for="email">Email</label>
              <input name="email" type="email" id="email" class="form-control" />
            </div>
        
            <!-- Password input -->
            <div class="form-outline mb-3">
              <label class="form-label" for="password">Password</label>
              <input name="password" type="password" id="password" class="form-control" />
            </div>

            <!-- Password conferma -->
            <div class="form-outline mb-4 pb-4">
              <label class="form-label" for="password_confirmation">Password conferma</label>
                <input name="password_confirmation" type="password" id="password_confirmation" class="form-control" />
            </div>
        
        
            <!-- Submit button -->
            <button type="submit" class="btn btn-light fontLora formBtn px-5">Registrati</button>
        
          </form>
  </div>

</x-form-layout>
