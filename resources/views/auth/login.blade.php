<x-form-layout>

  <div class="h-100 w-100 d-flex align-items-center justify-content-center">

    <form method="POST" action="{{route('login')}}"  class="myLoginForm w-75 fontLora p-4 m-auto">
      @csrf
      <h3 class="text-center">Login</h3>
        <!-- Email input -->
        <div class="form-outline mb-4">
          <label class="form-label" for="email">Email</label>
          <input name="email" type="email" id="email" class="form-control" />
        </div>
    
        <!-- Password input -->
        <div class="form-outline mb-4 pb-4">
          <label class="form-label" for="password">Password</label>
          <input name="password" type="password" id="password" class="form-control" />
        </div>
    
    
        <!-- Submit button -->
        <button type="submit" class="btn btn-light fontLora formBtn px-5">Accedi</button>
      </form>

  </div>

</x-form-layout>

