<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">

                <div class="fontLora text-center m-5">
                    <h1 class="fw-bold">The Aulab Post: giornale online indipendente</h1>
                    <hr>
                    <h3>"The Aulab Post è una piattaforma accessibile ad ogni utente perché
                        semplice da utilizzare, puoi trovare le notizie più recenti da tutto
                        il mondo su qualsiasi argomento, ciò accade perché la piattaforma permette a
                        chiunque di poter partecipare con noi come redattore e scrivere le notizie sul nostro portale"</h3>
                    <img src="{{asset('media/chisiamo1.jpg')}}" class="w-50 mt-5 rounded-2 shadow">
                </div>
            </div>
        </div>
    </div>

</x-layout>