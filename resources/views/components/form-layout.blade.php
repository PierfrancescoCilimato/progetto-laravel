<x-layout>

    <div class="container-fluid myBgImg">
    <div class="container">

        <div class="row justify-content-center">
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                <div class="min-vh-100 col-12 my-4">
                    {{$slot}}
                </div>
        </div>
    </div>
    </div>

</x-layout>
