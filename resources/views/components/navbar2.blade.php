<nav class="navbar p-0 navbar-expand-lg myBg2 shadow">


    
    <div class="container-fluid">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 mx-auto">
                <li class="nav-item mx-3 ">
                    <a class="nav-link text-dark fontLora navBtn p-0 m-2 fw-bold" href="{{ route('homepage') }}">Home</a>
                </li>

                <li class="nav-item mx-3 ">
                    <a class="nav-link text-dark fontLora navBtn p-0 m-2 fw-bold" href="{{route('chiSiamo')}}">Chi siamo</a>
                </li>

                <li class="nav-item mx-3 ">
                    <a class="nav-link text-dark fontLora navBtn p-0 m-2 fw-bold" href="{{ route('article.create') }}">Inserisci Articolo</a>
                </li>
                
                <li class="nav-item mx-3 ">
                    <a class="nav-link text-dark fontLora navBtn p-0 m-2 fw-bold" href="{{ route('careers') }}">Lavora con noi</a>
                </li>

            </ul>
            

        </div>
    </div>




</nav>

<nav class="navbar navbar-expand-lg navbar-light bg-light shadow" aria-label="Fourth navbar example">
    <div class="container-fluid">

        <button class="btn-category shadow navbar-toggler mx-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04"
            aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fontLora fw-bold">Categorie</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav mx-auto mb-2 mb-md-0">
                
                <li>
                    <a href="#" id="search_btn"><i class="bi bi-search fs-4 myColor1"></i></a>
                </li>

                @foreach ($categories as $category)
                    <li class="nav-item mx-auto">
                        <a class="btn btn-transparent mx-5 fs-6 fontLora fw-bold btnCat text-uppercase"
                            href="{{ route('article.byCategory', $category->id) }}">
                            {{ $category->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>