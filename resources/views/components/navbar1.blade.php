<nav class="navbar p-0 navbar-expand-lg myBg1 sticky-top shadow">


    
    <div class="container-fluid">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mb-2 mb-lg-0">

                <li class="nav-item me-2">
                    <p class="myColor1 fontLora p-0 m-2 fw-bold">{{date('d.m.Y')}}</p>
                </li>

            </ul>
            
            <p class="text-uppercase myColor1 fontLora p-0 m-2 fw-bold mx-auto">the aulab post</p>

            @Auth
            <div class="dropdown me-3 pe-4">
                <p class="navReg m-0 ms-2 mb-1 fontLora fs-5 ms-2 dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi bi-person-circle fs-3"></i> {{ Auth::user()->name }}</p>
                <ul class="dropdown-menu me-5">
                    <li>
                        @if (Auth::user()->is_admin)
                            <a class="btn btn-dark fontLora mx-2 dropdown-item" href="{{ route('admin.dashboard') }}">Dashboard Admin</a>
                        @endif
                    </li>
                    <li>
                        @if (Auth::user()->is_revisor)
                            <a class="btn btn-dark fontLora mx-2 dropdown-item" href="{{ route('revisor.dashboard') }}">Dashboard Revisore</a>
                        @endif
                    </li>
                    <li>
                        @if (Auth::user()->is_writer)
                            <a class="btn btn-dark fontLora mx-2 dropdown-item" href="{{ route('writer.dashboard') }}">Dashboard Redattore</a>
                        @endif
                    </li>
                  </ul>
            </div>
            
            <a class="btn btn-light text-dark fontLora navLog fw-bold" href="#" onclick="event.preventDefault();document.querySelector('#form-logout').submit()">Logout</a>
            <form class="d-none" id="form-logout" method="POST" action=" {{ route('logout') }}">@csrf</form>

            @else

                <a class="btn rounded-0 bg-transparent fw-bold border-0 m-2 fontLora myColor1 navBtn" href="{{ route('register') }}">Registrati</a>

                <a class="btn btn-light text-dark fontLora navLog fw-bold" href="{{ route('login') }}">Login</a>

            @endauth

        </div>
    </div>

</nav>
