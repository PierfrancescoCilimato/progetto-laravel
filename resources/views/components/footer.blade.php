<!-- Footer -->
<footer class="bgFooter text-center text-white fontLora">
  <!-- Grid container -->
  <div class="container p-4">

    <!-- Section: Form -->
    <section class="">
      <form action="">
        <!--Grid row-->
        <div class="row d-flex justify-content-center">
          <!--Grid column-->
          <div class="col-auto">
            <p class="pt-2">
              <strong>Iscriviti alla newsletter</strong>
            </p>
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-md-5 col-12">
            <!-- Email input -->
            <form class="form-outline form-white mb-4" action="{{route('newsletter.submit')}}" method="POST">
              @csrf
              <input type="email" id="email" name="email" class="form-control rounded-5" />
              <label class="form-label" for="email">Indirizzo Email</label>
            </form>
            
          </div>

          <!--Grid column-->
          <div class="col-auto">
            <!-- Submit button -->
            <button type="submit" class="btn btn-light fontLora text-dark fw-bold">
              Iscriviti
            </button>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </form>
    </section>

    <!-- Section: Links -->
    <section class="">
      <!--Grid row-->
      <div class="row mt-4">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <a href="{{route('chiSiamo')}}" class="text-white text-decoration-none text-uppercase"><h5>Chi Siamo</h5></a>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <a href="{{ route('careers') }}" class="text-white text-decoration-none text-uppercase"><h5>lavora con noi</h5></a>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <a href="{{route('wip')}}" class="text-white text-decoration-none text-uppercase"><h5>lorem</h5></a>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <a href="{{route('wip')}}" class="text-white text-decoration-none text-uppercase"><h5>lorem</h5></a>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </section>
    <!-- Section: Links -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2020 Copyright:
    <a class="text-white" href="#">Artisans Of Quality</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->