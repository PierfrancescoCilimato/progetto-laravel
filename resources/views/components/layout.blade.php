<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">  
    @if (isset($showArticleDetails) && $showArticleDetails)
    <meta name="keywords" content="@foreach($article->tags as $tag){{ $tag->name }}, @endforeach">
    @endif
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,500;1,400;1,500&display=swap" rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <title>The Aulab Post</title>

</head>
<body>

    <x-navbar1/>

    <x-navbar2/>

        <div class="min-vh-100">
            {{$slot}}
        </div>

    <x-footer/>

</body>
</html>