<nav class="navbar bg-transparent ">

  <div id="search_bar" class="w-75 mx-auto mt-3 getDis">
    <form class="d-flex mt-3" method="GET" action="{{route('article.search')}}">
      <input class="form-control mySearch " type="search" name="query" placeholder="Cosa stai cercando?" aria-label="Search">
      <button class="btnSearch fontLora px-4 navLog border-0" type="submit">Cerca</button>
    </form>
  </div>

    <div class="container-fluid d-flex justify-content-center border-bottom border-3 border-dark ">
      <h1 class="fontLora text-uppercase mt-5">{{$slot}}</h1>
    </div>

</nav>