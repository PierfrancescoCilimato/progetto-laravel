<div class="card p-3 myCard border-0 shadow h-100">

    <img src="{{ Storage::url($article->image) }}" class="card-img-top rounded-4 imgCard" alt="...">

    <div class="card-body d-flex flex-column justify-content-center pb-5">

        <div class="d-flex justify-content-between">
            <p class="fontLora cardHead">By <a href="{{ route('article.byUser', ['user' => $article->user->id]) }}"
                    class="card-text text-decoration-none text-muted">{{ $article->user->name }}</a></p>
            <p class="fontLora cardHead ">{{ $article->created_at->format('d/m/Y') }}</p>
        </div>
        @if ($article->category)
            <p class="fontLora cardHead fw-bold text-uppercase"><a
                    href="{{ route('article.byCategory', ['category' => $article->category->id]) }}"
                    class="card-text text-decoration-none text-muted">{{ $article->category->name }}</a>
            </p>
        @else
            <p class="fontLora cardHead fw-bold text-uppercase">
                Non categorizzato
            </p>
        @endif
        <h5 class="card-title fontLora">{{ $article->title }}</h5>
        <p class="fontLora subTitle"> {{ Str::limit($article->subtitle, 100, '...') }} </p>
        <p class="small fst-italic text-capitalize">
            @foreach ($article->tags as $tag)
                #{{ $tag->name }}
            @endforeach
        </p>
        <hr>
        <span class="text-muted small fst-italic">- tempo di lettura @if($article->readDuration() == 0) 1 @else{{$article->readDuration()}} @endif min</span>
        
        <a href="{{route('article.show', compact('article')) }}" class="btn btn-dark fontLora cardBtn">Leggi
            Articolo
        </a>

    </div>

</div>
