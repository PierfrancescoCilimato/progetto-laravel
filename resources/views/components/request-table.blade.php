<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($roleRequests as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @switch($role)
                        @case('amministratore')
                            <a href="{{ route('admin.setAdmin', compact('user')) }}" class="btn btn-outline-dark fw-bold">Attiva
                                {{ $role }}</a>
                        @break

                        @case('revisore')
                            <a href="{{ route('admin.setRevisor', compact('user')) }}" class="btn btn-outline-dark fw-bold">Attiva
                                {{ $role }}</a>
                        @break

                        @case('redattore')
                            <a href="{{ route('admin.setWriter', compact('user')) }}" class="btn btn-outline-dark fw-bold">Attiva
                                {{ $role }}</a>
                        @break
                    @endswitch
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
